package fr.grislain.benchmark;

public class MultpleAdHocPolymorphism {
	
	public static abstract class A {
		public abstract void run(B b);
	}
	public static class A1 extends A {
		@Override public void run(B b) {
			b.run(AB.ab().a(this));
		}
	}
	public static class A2 extends A {
		@Override public void run(B b) {
			b.run(AB.ab().a(this));
		}
	}
	
	public static abstract class B {
		public abstract void run(AB ab);
	}
	public static class B1 extends B {
		@Override public void run(AB ab) {
			ab.b(this).run();
		}
	}
	public static class B2 extends B {
		@Override public void run(AB ab) {
			ab.b(this).run();
		}
	}
	
	public static class AB {
		public void run() {System.out.println("-");}
		public static AB ab() {return new AB();}
		public AB a(A1 a) {
			return new AB() {
				public AB b(B1 b) {
					return new AB() {
						public void run() {System.out.println("A1B1");}
					};
				}
				public AB b(B2 b) {
					return new AB() {
						public void run() {System.out.println("A1B2");}
					};
				}
			};
		}
		public AB a(A2 a) {
			return new AB() {
				public AB b(B1 b) {
					return new AB() {
						public void run() {System.out.println("A2B1");}
					};
				}
				public AB b(B2 b) {
					return new AB() {
						public void run() {System.out.println("A2B2");}
					};
				}
			};
		}
		public AB b(B1 b) {return null;}
		public AB b(B2 b) {return null;}
	}
	
	
	public static void main(String[] args) {
		A a = new A1();
		B b = new B2();
		
		a.run(b);
	}

}
