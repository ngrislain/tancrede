package fr.grislain.benchmark;

import java.util.stream.IntStream;

public class ArrayBenchmark {
	public static void main(String[] args) {
		long time = 0;
		final int n = 200 ;//000000;
		double[] data = new double[n];
		double[] result = new double[n];
		
		time = System.currentTimeMillis();
		for (int i=0; i<data.length; i++) {
			data[i] = 1;
		}
		System.out.println(String.format("%d msec to fill with 1", System.currentTimeMillis()-time));
		System.out.println(data[0]);
		
		time = System.currentTimeMillis();
		IntStream.range(0,data.length).forEach(i -> {data[i] = 2;});
		System.out.println(String.format("%d msec to fill with 2", System.currentTimeMillis()-time));
		System.out.println(data[0]);
		
	}
}
