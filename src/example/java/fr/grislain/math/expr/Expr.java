package fr.grislain.math.expr;

import java.util.function.Function;

public interface Expr<R> {
	R eval(Function<String,R> context);
	/** Const */
	public static class Const<R> implements Expr<R> {
		private final R value;
		public Const(R value) {
			this.value = value;
		}
		@Override public R eval(Function<String, R> context) {return value;}
	}
	/** Var */
	public static class Var<R> implements Expr<R> {
		private final String name;
		public Var(String name) {
			this.name = name;
		}
		@Override public R eval(Function<String, R> context) {return context.apply(name);}
	}
}
