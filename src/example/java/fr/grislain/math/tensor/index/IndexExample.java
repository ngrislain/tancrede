package fr.grislain.math.tensor.index;

import java.util.Arrays;

/** An Index iterator */
public final class IndexExample {
	public static void main(String[] args) {
		for (int[] index : Indices.index(2,3,5)) {
			System.out.println(Arrays.toString(index));
		}
	}
}
