package fr.grislain.math.tensor.index;

import java.util.Arrays;

import fr.grislain.math.tensor.index.iterable.Struct;

/** An Index iterator */
public final class StructExample {
	public static void main(String[] args) {
		int[] shape = new int[]{2,3,5};
		for (Struct.Element e : Indices.struct(shape)) {
			System.out.println(e.type.name()+" "+e.depth+" "+Arrays.toString(e.indices));
		}
	}
}
