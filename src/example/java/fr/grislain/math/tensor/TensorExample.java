package fr.grislain.math.tensor;

import fr.grislain.math.tensor.Dense;
import fr.grislain.math.tensor.index.Broadcast;

import static fr.grislain.math.tensor.index.Indices.*;

public class TensorExample {
	public static void main(String[] args) {
		Dense t = Tensors.dense(2,3,5);
		Sparse s = Tensors.sparse(2,3,5);
		
		t.set(0.5,1,1,1);
		t.set(1,0,1).apply(3.);
		System.out.println(t.map(v -> 2*v+1));
		
		System.out.println();
		System.out.println(Tensors.dense(3,5).set(10., 1,1));

		System.out.println();
		System.out.println(t);
		
		System.out.println(t.get(a(1,1).a()));
		
		s.set(0.5,1,1,1);
		s.set(1,0,1).apply(3.);
		System.out.println();
		System.out.println(s);
		System.out.println(s.data());
		
		// Scalar tensor example
		Dense u = Tensors.dense();
		u.set(10.3);
		System.out.println(u.get());
		
		t = Tensors.dense(5);
		t.set(1.0, 1).set(2.0, 4);
		System.out.println("t="+t);
		// Broadcast exemple
		System.out.println(t.broadcast(3,5,7));
	}
}
