package fr.grislain.async.node;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import fr.grislain.async.message.Message;

public interface Node extends Runnable, Closeable {
	List<Address> neighbors();
	<T> void send(Message<T> message, Address address);
	<T> void receive(Message<T> message);
	// A simple implementation
	public static abstract class Implementation implements Node {
		private final ByteBuffer buffer = ByteBuffer.allocate(1<<16);
		private final AsynchronousSocketChannel channel;
		private final Map<Address, AsynchronousSocketChannel> neighborChannels = new HashMap<>();
		/** The constructor register the new node in a central registry */
		public Implementation() {
			try {
				List<Future<Void>> futureConnections = new ArrayList<>();
				channel = AsynchronousServerSocketChannel.open().bind(Address.provider().address()).accept().get();
				for (Address neighbor : neighbors()) {
					AsynchronousSocketChannel asynchronousSocketChannel = AsynchronousSocketChannel.open();
					neighborChannels.put(neighbor, asynchronousSocketChannel);
					futureConnections.add(asynchronousSocketChannel.connect(neighbor));
				}
				for (Future<Void> futureConnection : futureConnections) {
					futureConnection.get();
				}
			} catch (IOException|ExecutionException|InterruptedException e) {throw new RuntimeException(e);}
		}
		@Override public <T> void send(Message<T> message, Address address) {
			neighborChannels.get(address).write(buffer);
		}
		@Override public <T> void receive(Message<T> message) {
			
		}
		@Override public void close() throws IOException {
			channel.close();
			for (AsynchronousSocketChannel channel : neighborChannels.values()) {
				channel.close();
			}
		}
		
	}
}
