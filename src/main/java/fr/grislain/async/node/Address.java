package fr.grislain.async.node;

import java.net.InetAddress;
import java.net.InetSocketAddress;

public class Address extends InetSocketAddress {
	private static final long serialVersionUID = -1746706407631531517L;
	// Constructors
	public Address(int port) {super(port);}
	public Address(InetAddress addr, int port) {super(addr, port);}
	public Address(String hostname, int port) {super(hostname, port);}
	/** An address provider */
	public static Provider provider() {return Provider.instance;}
	public static class Provider {
		/** Singleton instance */
		private static final Provider instance = new Provider();
		/** First port available */
		private int next = (1<<15)+(1<<14);
		private Provider() {}
		public Address address() {
			return new Address(next++);
		}
	}
}
