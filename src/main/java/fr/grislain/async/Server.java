package fr.grislain.async;

import java.net.InetSocketAddress;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class Server implements Runnable {
	private final int port;
	public Server(int port) {
		this.port = port;
	}
	private final EventLoopGroup parentGroup = new NioEventLoopGroup();
	private final EventLoopGroup childGroup = new NioEventLoopGroup();
	public void run() {
		System.out.println("Starting server");
		try {
			ServerBootstrap serverBootstrap = new ServerBootstrap();
			serverBootstrap.group(parentGroup, childGroup)
			.channel(NioServerSocketChannel.class)
			.localAddress(new InetSocketAddress(port))
			.childHandler(new ChannelInitializer<SocketChannel>() {
				@Override
				public void initChannel(SocketChannel channel) throws Exception {
					channel.pipeline().addLast(new Handler());
				}
			});
			ChannelFuture channelFuture = serverBootstrap.bind().sync();
			channelFuture.channel().closeFuture().sync();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				parentGroup.shutdownGracefully().sync();
				childGroup.shutdownGracefully().sync();
			}
			catch (InterruptedException e) {throw new RuntimeException(e);}
		}
	}
	
	/** The request handler */
	@Sharable
	public static class Handler extends ChannelInboundHandlerAdapter {
		@Override
		public void channelRead(ChannelHandlerContext context, Object message) {
			System.out.println("Server received " + message);
			context.write(message);
		}
		@Override
		public void channelReadComplete(ChannelHandlerContext context) {
			context.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
		}
		@Override
		public void exceptionCaught(ChannelHandlerContext context, Throwable cause) {
			cause.printStackTrace();
			context.close();
		}
	}
}
