package fr.grislain.async;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import fr.grislain.async.node.Address;

/*
 * https://raft.github.io/
 * http://research.microsoft.com/en-us/um/people/lamport/pubs/paxos-simple.pdf
 */

public class Paxos {
	public static void main(String[] args) throws IOException {
		Address.provider().address();
		Address.provider().address();
		System.out.println(Address.provider().address());
		
		Enumeration e = NetworkInterface.getNetworkInterfaces();
		while(e.hasMoreElements())
		{
		    NetworkInterface n = (NetworkInterface) e.nextElement();
		    Enumeration ee = n.getInetAddresses();
		    while (ee.hasMoreElements())
		    {
		        InetAddress i = (InetAddress) ee.nextElement();
		        System.out.println(i.getHostAddress());
		        System.out.println(i.isLoopbackAddress());
		        System.out.println(i.isSiteLocalAddress());
		        System.out.println(i.isReachable(100));
		    }
		}
		
	}
}
