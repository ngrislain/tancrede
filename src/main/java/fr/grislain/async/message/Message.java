package fr.grislain.async.message;

import java.nio.ByteBuffer;

import fr.grislain.async.node.Node;

public interface Message<T> {
	int type();
	Node from();
	Node to();
	T content();
	ByteBuffer write(ByteBuffer bytes);
	Message<T> read(ByteBuffer bytes);
}
