package fr.grislain.async;

import java.net.InetSocketAddress;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.CharsetUtil;


public class Client implements Runnable {
	private final String host;
	private final int port;
	private final EventLoopGroup group = new NioEventLoopGroup();

	public Client(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public void run() {
		System.out.println("Starting client");
		try {
			Bootstrap bootstrap = new Bootstrap();
			bootstrap.group(group)
				.channel(NioSocketChannel.class)
				.remoteAddress(new InetSocketAddress(host, port))
				.handler(new ChannelInitializer<SocketChannel>() {
					@Override
					public void initChannel(SocketChannel channel) throws Exception {
						channel.pipeline().addLast(new Handler());
					}
				});
			ChannelFuture channelFuture = bootstrap.connect().sync();
			System.out.println("Client connected");
			channelFuture.channel().closeFuture().sync();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} finally {
			try {group.shutdownGracefully().sync();}
			catch (InterruptedException e) {throw new RuntimeException(e);}
		}
	}
	
	@Sharable
	public static class Handler extends SimpleChannelInboundHandler<ByteBuf> {
		@Override
		public void channelActive(ChannelHandlerContext context) {
			context.write(Unpooled.copiedBuffer("Netty rocks!", CharsetUtil.UTF_8));
			context.flush();
		}
		@Override
		public void channelRead0(ChannelHandlerContext context, ByteBuf in) {
			System.out.println("Client received: " + in.readBytes(in.readableBytes()));
		}
		@Override
		public void exceptionCaught(ChannelHandlerContext context, Throwable cause) {
			cause.printStackTrace();
			context.close();
		}
	}
}
