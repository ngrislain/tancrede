package fr.grislain.math.tensor;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import fr.grislain.math.tensor.index.Broadcast;
import fr.grislain.math.tensor.index.Index;
import fr.grislain.math.tensor.index.Indices;
import fr.grislain.math.tensor.index.Interval;
import fr.grislain.math.tensor.index.iterable.Struct;

public interface Tensor<T extends Tensor<T>> {
	/** The shape */
	int[] shape();
	/** Returns the rank of the tensor */
	default int rank() {return shape().length;}
	/** Returns the dimension of the tensor */
	default int dim() {return Indices.dim(shape());}
	/** Get a value */
	double get(int... indices);
	
	/** Get a sub-tensor from an index */
	@SuppressWarnings("unchecked")
	default CoFunctor<?,T> get(Index index) {
		return new CoFunctor<>((T)this, index);
	}
	/** Get a sub-tensor from an interval builder */
	@SuppressWarnings("unchecked")
	default CoFunctor<?,T> get(Interval.Builder interval) {
		return get(interval.interval());
	}
	@SuppressWarnings("unchecked")
	default CoFunctor<?,T> broadcast(int... shape) {
		return get(new Broadcast(shape, this.shape()));
	}
	
	/** Map a function on the tensor (default implementation) */
	public default Functor map(Function<Double, Double> function) {return new Functor(this, function);}
	
	/** Traverse the tensor with minimum work */
	@SuppressWarnings("unchecked")
	default T forEach(Consumer<Double> action) {
		for (int[] indices : Indices.index()) {action.accept(get(indices));}
		return (T)this;
	}
	
	/** Traverse the tensor with some more work */
	@SuppressWarnings("unchecked")
	default T forEach(Consumer<Double> onValue,
			Consumer<Integer> onStart,
			Consumer<Integer> onNext,
			Consumer<Integer> onEnd) {
		Struct.Element.Type last = Struct.Element.Type.NULL;
		for (Struct.Element element : Indices.struct(shape())) {
			switch (element.type) {
			case START:
				if (last==Struct.Element.Type.END) onNext.accept(element.depth-1);
				onStart.accept(element.depth);
				break;
			case VALUE:
				if (last==Struct.Element.Type.VALUE) onNext.accept(element.depth);
				onValue.accept(get(element.indices));
				break;
			case END:
				onEnd.accept(element.depth);
				break;
			default:
				break;
			}
			last = element.type;
		}
		return (T)this;
	}
	/** Traverse a tensors and write a transformed version in result */
	public default <M extends Mutable<?>> M forEach(Function<Double,Double> op, M result) {
		for (int[] indices : Indices.index()) {
			result.set(op.apply(this.get(indices)), indices);
		}
		return result;
	}
	/** Traverse two tensors and write the result of an op in result */
	public default <M extends Mutable<?>> M forEach(BiFunction<Double,Double,Double> op, T that, M result) {
		for (int[] indices : Indices.index()) {
			result.set(op.apply(this.get(indices), that.get(indices)), indices);
		}
		return result;
	}
	/** Traverse a tensors and write a transformed version in result */
	public default Mutable<?> forEach(Function<Double,Double> op) {
		return forEach(op, new Dense(this.shape()));
	}
	/** Traverse two tensors and return the result of an op as a new Dense Tensor */
	public default Mutable<?> forEach(BiFunction<Double,Double,Double> op, T that) {
		return forEach(op, that, new Dense(this.shape()));
	}
	/* Add */
	public default <M extends Mutable<?>> M add(T that, M result) {return forEach((x,y)->x+y, that, result);}
	public default Mutable<?> add(T that) {return forEach((x,y)->x+y, that);}
	/* Sub */
	public default <M extends Mutable<?>> M sub(T that, M result) {return forEach((x,y)->x-y, that, result);}
	public default Mutable<?> sub(T that) {return forEach((x,y)->x-y, that);}
	/* Reverse sub */
	public default <M extends Mutable<?>> M rsub(T that, M result) {return forEach((x,y)->y-x, that, result);}
	public default Mutable<?> rsub(T that) {return forEach((x,y)->y-x, that);}
	/* Mul */
	public default <M extends Mutable<?>> M mul(T that, M result) {return forEach((x,y)->x*y, that, result);}
	public default Mutable<?> mul(T that) {return forEach((x,y)->x*y, that);}
	/* Div */
	public default <M extends Mutable<?>> M div(T that, M result) {return forEach((x,y)->x/y, that, result);}
	public default Mutable<?> div(T that) {return forEach((x,y)->x/y, that);}
	/* Reverse div */
	public default <M extends Mutable<?>> M rdiv(T that, M result) {return forEach((x,y)->y/x, that, result);}
	public default Mutable<?> rdiv(T that) {return forEach((x,y)->y/x, that);}
	
	/** A mutable tensor interface */
	public interface Mutable<M extends Tensor.Mutable<M>> extends Tensor<M> {
		public M set(double value, int... indices);
		/** Set a value in a curryfied way */
		public default Function<Double,M> set(int... indices) {
			return value -> {
				return set(value, indices);
			};
		}
		/** Get a sub-tensor from an index */
		@SuppressWarnings("unchecked")
		default CoFunctor<?,M> get(Index index) {
			return new CoFunctor<>((M)this, index);
		}
		/** Get a sub-tensor from an interval builder */
		@SuppressWarnings("unchecked")
		default CoFunctor<?,M> get(Interval.Builder interval) {
			return get(interval.interval());
		}
		/** Get a super-tensor from a broadcast */
		@SuppressWarnings("unchecked")
		default CoFunctor<?,M> broadcast(int... shape) {
			return get(new Broadcast(shape, this.shape()));
		}
		/** Traverse a tensors and write a transformed version in result */
		@SuppressWarnings("unchecked")
		public default M forEachInPlace(Function<Double,Double> op) {
			for (int[] indices : Indices.index()) {
				set(op.apply(get(indices)), indices);
			}
			return (M)this;
		}
		/** Traverse two tensors and return the result of an op as a new Dense Tensor */
		@SuppressWarnings("unchecked")
		public default M forEachInPlace(BiFunction<Double,Double,Double> op, M that) {
			for (int[] indices : Indices.index()) {
				set(op.apply(get(indices), that.get(indices)), indices);
			}
			return (M)this;
		}
		/* Add */
		public default M addi(M that) {return forEachInPlace((x,y)->x+y, that);}
		/* Sub */
		public default M subi(M that) {return forEachInPlace((x,y)->x-y, that);}
		/* Reverse sub */
		public default M rsubi(M that) {return forEachInPlace((x,y)->y-x, that);}
		/* Mul */
		public default M muli(M that) {return forEachInPlace((x,y)->x*y, that);}
		/* Div */
		public default M divi(M that) {return forEachInPlace((x,y)->x/y, that);}
		/* Reverse div */
		public default M rdivi(M that) {return forEachInPlace((x,y)->y/x, that);}
	}
}
