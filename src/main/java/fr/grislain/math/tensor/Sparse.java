package fr.grislain.math.tensor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import fr.grislain.math.tensor.index.Indices;

// TODO: reimplement foreach op

/** A dense implementation */
public class Sparse implements Tensor<Sparse>, Tensor.Mutable<Sparse> {
	private final int dim;
	private final Map<Key,Double> data = new HashMap<Key,Double>();
	private int rank;
	private int[] shape;
	
	/** Constructor from a shape */
	public Sparse(int... shape) {
		this.shape = shape;
		rank = shape.length;
		dim = Indices.dim(shape);
	}
	
	@Override public int rank() {return rank;}
	@Override public int dim() {return dim;}
	/** Build a defensive copy */
	@Override public int[] shape() {return Arrays.copyOf(shape, rank);}
	public Map<Key,Double> data() {return data;}
	@Override public double get(int... indices) {
		Double result = data.get(key(indices));
		return result==null ? 0 : result;
	}
	@Override public Sparse set(double value, int... indices) {
		data.put(key(indices), value);
		return this;
	}
	@Override
	public String toString() {
		return Tensors.toString(this);
	}
	/** A key type to wrap int[] properly for HashMaps */
	public static class Key {
		private final int[] key;
		public Key(int... key) {this.key = key;}
		public int[] key() {return key;}
		@Override public int hashCode() {
			return Arrays.hashCode(key);
		}
		@Override public boolean equals(Object obj) {
			return Arrays.equals(key, ((Key)obj).key);
		}
		@Override public String toString() {
			return Arrays.toString(key);
		}
	}
	public static Key key(int... key) {return new Key(key);}
	/** Traverse a tensors and write a transformed version in result */
	public Sparse forEach(Function<Double,Double> op) {
		return forEach(op, new Sparse(this.shape()));
	}
	/** Traverse two tensors and return the result of an op as a new Dense Tensor */
	public Sparse forEach(BiFunction<Double,Double,Double> op, Sparse that) {
		return forEach(op, that, new Sparse(this.shape()));
	}
}
