package fr.grislain.math.tensor;

/**
 * Utilities for tensors
 * @author nicolas
 *
 */
public class Tensors {
	/** Static implementation for toString */
	public static String toString(Tensor<?> tensor) {
		final int MAX = 1000;
		final StringBuilder result = new StringBuilder();
		if (tensor.dim()>MAX) throw new IllegalArgumentException("Tensor too large!");
		tensor.forEach(value -> {result.append(value);},
				depth -> {
					if (depth<=tensor.rank()) result.append("[");
				},
				depth -> {
					if (depth==tensor.rank()) {
						result.append("\t");
					} else {
						result.append("\n");
						for (int i=0; i<=depth; i++) {
							result.append(" ");
						}
					}
				},
				depth -> {
					if (depth<tensor.rank()) result.append("]");
				});
		return result.toString();
	}
	
	/* Builders */
	public static Dense dense(int... indices) {return new Dense(indices);}
	public static Sparse sparse(int... indices) {return new Sparse(indices);}
}
