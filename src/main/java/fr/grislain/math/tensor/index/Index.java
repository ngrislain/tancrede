package fr.grislain.math.tensor.index;

import java.util.function.Function;

public interface Index extends Function<int[], int[]> {
	public int[] domain(int[] coDomain);
}
