package fr.grislain.math.tensor.index;

public class Broadcast implements Index {
	private final int[] domain;
	private final int[] coDomain;
	private final int[] index;
	public Broadcast(int[] domain, int[] coDomain) {
		this.domain = domain;
		this.coDomain = coDomain;
		index = new int[coDomain.length];
	}
	@Override public int[] apply(int[] index) {
		int j=0;
		for (int i=0; i<domain.length; i++) {
			if (j<coDomain.length && domain[i] == coDomain[j]) {
				this.index[j++] = index[i];
			}
		}
		return this.index;
	}
	@Override public int[] domain(int[] coDomain) {return domain;}

}
