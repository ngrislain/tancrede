package fr.grislain.math.tensor.index.iterable;

import java.util.Iterator;

import fr.grislain.math.tensor.index.Indices;

/** An Index iterator */
public final class Index implements Iterable<int[]> {
	private final int[] shape;
	private final int rank;
	private final int dim;
	
	/** Constructor */
	public Index(int... shape) {
		this.shape = shape;
		rank = shape.length;
		dim = Indices.dim(shape);
	}
	
	public int[] shape() {return shape;}
	public int rank() {return rank;}
	public int dim() {return dim;}
	
	@Override public Iterator<int[]> iterator() {
		return new Iterator<int[]>() {
			private final int[] indices = new int[rank];
			private boolean next = true;
			private int delta = 0;
			private void setNext() {
				if (!next) {
					delta = Index.next(shape, indices);
					next = true;
				}
			}
			@Override public boolean hasNext() {
				setNext();
				return delta>=0;
			}
			@Override public int[] next() {
				setNext();
				next = false;
				return indices;
			}
		};
	}
	
	/** A routine to compute the next indices from the current ones, given the shape, return the index updated */
	public static int next(int[] shape, int[] index) {
		int i = shape.length-1;
		index[i] += 1;
		while (index[i]>=shape[i] && i>0) {
			index[i] = 0;
			index[i-1] += 1;
			i--;
		}
		if (index[i]>=shape[i]) {
			index[i] = 0;
			i--;
		}
		return i;
	}
}
