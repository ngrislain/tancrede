package fr.grislain.math.tensor.index;

import java.util.Arrays;

import fr.grislain.math.tensor.index.iterable.Index;
import fr.grislain.math.tensor.index.iterable.Struct;

public class Indices {
	/** A routine to compute the dimension from the shape */
	public static final int dim(int[] shape) {return Arrays.stream(shape).reduce(1,(x,y) -> x*y);}
	/** A builder for Index */
	public static Index index(int... shape) {return new Index(shape);}
	/** A builder for Struct */
	public static Struct struct(int... shape) {return new Struct(shape);}
	/** Interval creation */
	public static Interval.Builder a() {return new Interval.Builder().a();}
	public static Interval.Builder a(int... values) {return new Interval.Builder().a(values);}
}
