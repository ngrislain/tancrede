package fr.grislain.math.tensor.index.iterable;

import java.util.Iterator;

import fr.grislain.math.tensor.index.Indices;

public final class Struct implements Iterable<Struct.Element> {
	private final int[] shape;
	private final int rank;
	private final int dim;
	/** Constructor */
	public Struct(int... shape) {
		this.shape = shape;
		rank = shape.length;
		dim = Indices.dim(shape);
	}
	public int[] shape() {return shape;}
	public int rank() {return rank;}
	public int dim() {return dim;}
	@Override public Iterator<Element> iterator() {
		return new Iterator<Element>() {
			private final Element element = new Element(rank);
			private boolean next = false;
			private void setNext() {
				if (!next) {
					Struct.next(shape, element);
					next = true;
				}
			}
			@Override public boolean hasNext() {
				setNext();
				return element.type != Element.Type.NULL;
			}
			@Override public Element next() {
				setNext();
				next = false;
				return element;
			}
		};
	}
	
	/** An element of struct */
	public static class Element {
		public static enum Type {NULL, START, VALUE, END};
		public Type type = Type.NULL;
		public int depth;
		public int[] indices;
		public Element(int rank) {
			indices = new int[rank];
		}
	}
	
	/** A low-level routine to compute the next step of structural traversal, given the shape, return the index updated */
	public static final void next(int[] shape, Element struct) {
		switch (struct.type) {
		case NULL:
			struct.depth=0;
			if (struct.depth<shape.length) {
				struct.type = Element.Type.START;
				struct.depth++;
			}
			return;
		case START:
			if (struct.depth<shape.length) {
				struct.type = Element.Type.START;
				struct.depth++;
			} else {
				struct.type = Element.Type.VALUE;
				struct.indices[struct.depth-1]=0;
			}
			return;
		case VALUE:
			if (struct.indices[struct.depth-1]<shape[struct.depth-1]-1) {
				struct.type = Element.Type.VALUE;
				struct.indices[struct.depth-1]++;
			} else {
				struct.type = Element.Type.END;
				struct.indices[struct.depth-1]=0;
				struct.depth--;
			}
			return;
		case END:
			if (struct.depth>0) {
				struct.indices[struct.depth-1]++;
				if (struct.indices[struct.depth-1]<shape[struct.depth-1]) {
					struct.type = Element.Type.START;
					struct.depth++;
				} else {
					struct.type = Element.Type.END;
					struct.indices[struct.depth-1]=0;
					struct.depth--;
				}
			} else {
				struct.type = Element.Type.NULL;
			}
			return;
		}
	}
}
