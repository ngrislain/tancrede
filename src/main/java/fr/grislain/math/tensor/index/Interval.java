package fr.grislain.math.tensor.index;

import java.util.Arrays;

/** A trivial implementation of the identity */
public class Interval implements Index {
	public static final int ALL = Integer.MAX_VALUE;
	private final int[] start;
	private final int[] end;
	private final int[] index;
	private final int[] domain;
	public Interval(int[] start, int[] end) {
		this.start = start;
		this.end = end;
		index = new int[start.length];
		int rank = 0;
		for (int i=0; i<start.length; i++) {
			if (end[i]>start[i]+1) {
				rank++;
			}
		}
		domain = new int[rank];
	}
	@Override public int[] apply(int[] index) {
		int j = 0;
		for (int i=0; i<start.length; i++) {
			if (end[i]==start[i]+1) {
				this.index[i] = start[i];
			} else {
				this.index[i] = start[i]+index[j++];
			}
		}
		return this.index;
	}
	@Override public int[] domain(int[] coDomain) {
		int j = 0;
		for (int i=0; i<start.length; i++) {
			if (end[i]>start[i]+1) {
				domain[j++] = Math.min(end[i],coDomain[i])-start[i];
			}
		}
		return domain;
	}
	public static class Builder {
		private int n = 32;
		private int i = 0;
		private int[] start = new int[n];
		private int[] end = new int[n];
		/** Select a range for the next dimension */
		public Builder a(int start, int end) {
			if (i<n) {
				this.start[i] = start;
				this.end[i] = end;
			} else {
				n <<= 1;
				this.start = Arrays.copyOf(this.start, n);
				this.end = Arrays.copyOf(this.end, n);
				this.start[i] = start;
				this.end[i] = end;
			}
			i++;
			return this;
		}
		/** Select a range for the next dimension */
		public Builder a(int... values) {
			for (int value : values) {
				a(value, value+1);
			}
			return this;
		}
		public Builder a() {
			return a(0, ALL);
		}
		public Interval interval() {return new Interval(Arrays.copyOf(start,i), Arrays.copyOf(end,i));}
	}
}
