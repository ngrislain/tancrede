package fr.grislain.math.tensor;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Function;

import fr.grislain.math.tensor.index.Indices;

/** A dense implementation */
public class Dense implements Tensor<Dense>, Tensor.Mutable<Dense> {
	private final int dim;
	private final double[] data;
	private int rank;
	private int[] shape;
	
	/** Constructor from a shape */
	public Dense(int... shape) {
		this.shape = shape;
		rank = shape.length;
		dim = Indices.dim(shape);
		data = new double[dim];
	}
	
	@Override public int rank() {return rank;}
	@Override public int dim() {return dim;}
	/** Build a defensive copy */
	@Override public int[] shape() {return Arrays.copyOf(shape, rank);}
	public double[] data() {return data;}
	
	/** Efficiently computes an index */
	public final int index(int... indices) {
		int index = 0;
		int step = dim;
		for (int i=0; i<rank; i++) {
			if (indices[i]<0 || indices[i]>=shape[i]) throw new IndexOutOfBoundsException(Arrays.toString(indices));
			step /= shape[i];
			index += indices[i]*step;
		}
		return index;
	}
	@Override
	public double get(int... indices) {
		return data[index(indices)];
	}
	@Override
	public Dense set(double value, int... indices) {
		data[index(indices)] = value;
		return this;
	}
	@Override
	public Function<Double,Dense> set(int... indices) {
		return value -> {
			data[index(indices)] = value;
			return this;
		};
	}
	@Override
	public String toString() {
		return Tensors.toString(this);
	}
	/** Traverse a tensors and write a transformed version in result */
	public Dense forEach(Function<Double,Double> op) {
		return forEach(op, new Dense(this.shape()));
	}
	/** Traverse two tensors and return the result of an op as a new Dense Tensor */
	public Dense forEach(BiFunction<Double,Double,Double> op, Dense that) {
		return forEach(op, that, new Dense(this.shape()));
	}
}
