package fr.grislain.math.tensor;

import java.util.Arrays;

import fr.grislain.math.tensor.index.Index;
import fr.grislain.math.tensor.index.Indices;

public class CoFunctor<C extends CoFunctor<C,T>, T extends Tensor<T>> implements Tensor<C> {
	protected final T tensor;
	protected final Index index;
	private final int[] shape;
	private final int dim;
	private final int rank;
	
	/** The constructor require the shape as the codomain of function may be non-trivial */
	public CoFunctor(T tensor, Index index) {
		this.tensor = tensor;
		this.index = index;
		this.shape = index.domain(tensor.shape());
		rank = shape.length;
		dim = Indices.dim(shape);
	}
	@Override public int rank() {return rank;}
	@Override public int dim() {return dim;}
	/** Build a defensive copy */
	@Override public int[] shape() {return Arrays.copyOf(shape, rank);}
	@Override public double get(int... indices) {
		return tensor.get(index.apply(indices));
	}
	@Override public String toString() {
		return Tensors.toString(this);
	}
	public static class Mutable<C extends CoFunctor.Mutable<C,T>, T extends Tensor.Mutable<T>> extends CoFunctor<C,T> implements Tensor.Mutable<C> {
		public Mutable(T tensor, Index index) {super(tensor, index);}
		@SuppressWarnings("unchecked")
		@Override
		public C set(double value, int... indices) {
			tensor.set(value, index.apply(indices));
			return (C)this;
		}
	}
}
