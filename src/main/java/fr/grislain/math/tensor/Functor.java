package fr.grislain.math.tensor;

import java.util.Arrays;
import java.util.function.Function;

import fr.grislain.math.tensor.index.Indices;

/** A functor class to streamline the map operation */
public class Functor implements Tensor {
	private final Tensor tensor;
	private final Function<Double, Double> function;
	private final int dim;
	private final int rank;
	private final int[] shape;
	
	/** A simple constructor */
	public Functor(Tensor tensor, Function<Double, Double> function) {
		this.tensor = tensor;
		this.function = function;
		shape = tensor.shape();
		rank = shape.length;
		dim = Indices.dim(shape);
	}
	@Override public int rank() {return rank;}
	@Override public int dim() {return dim;}
	/** Build a defensive copy */
	@Override public int[] shape() {return Arrays.copyOf(shape, rank);}
	@Override public double get(int... indices) {
		return function.apply(tensor.get(indices));
	}
	@Override public String toString() {
		return Tensors.toString(this);
	}
}
