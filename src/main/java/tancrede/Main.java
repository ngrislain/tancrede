package tancrede;

import fr.grislain.async.Client;
import fr.grislain.async.Server;

public class Main {
	public static void main(String[] args) throws InterruptedException  {
		System.out.println("Tancrede");
		
		new Thread(new Server(8080)).start();
		
		Thread.sleep(3000);
		new Thread(new Client("localhost", 8080)).start();
	}	
}