import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

a = 0.2
b = 3.9

f = lambda x: ((b-a)+a*np.random.rand())*x*(1-x)
n = 1000
s = [0.9]
for i in xrange(n):
    s.append(f(s[-1]))

# plt.plot(s)
# plt.show()

x_data = np.array(s[:-1]).reshape((n,1))
y_data = np.array(s[1:]).reshape((n,1))

# plt.plot(x_data, y_data, 'ro')
# plt.show()

sess = tf.InteractiveSession()

x = tf.placeholder(tf.float32, shape=[None, 1])
y_ = tf.placeholder(tf.float32, shape=[None, 1])

W_1 = tf.Variable(tf.truncated_normal([1, 50], stddev=0.1))
b_1 = tf.Variable(tf.constant(0.1, shape=[50]))
h_1 = tf.nn.relu(tf.add(tf.matmul(x, W_1), b_1))

W_2 = tf.Variable(tf.truncated_normal([50, 50], stddev=0.1))
b_2 = tf.Variable(tf.constant(0.1, shape=[50]))
h_2 = tf.nn.relu(tf.add(tf.matmul(h_1, W_2), b_2))

W_3 = tf.Variable(tf.truncated_normal([50, 1], stddev=0.1))
b_3 = tf.Variable(tf.constant(0.1, shape=[1]))
y = tf.nn.relu(tf.add(tf.matmul(h_2, W_3), b_3))


error = tf.reduce_sum((y_-y)*(y_-y))

train_step = tf.train.AdamOptimizer(learning_rate=1e-3, epsilon=1e-12).minimize(error)

sess.run(tf.initialize_all_variables())
n_iter = 10000
err = []
for i in xrange(n_iter):
    train_step.run(feed_dict={x: x_data, y_: y_data})
    err.append(error.eval(feed_dict={x: x_data, y_: y_data}))
    if i%1000 == 0:
        print i
    if i%5000 == 0:
        plt.plot(err[-5000:])
        plt.show()

print y.eval(feed_dict={x: x_data})

plt.plot(y.eval(feed_dict={x: x_data}), y_data, 'ko')
plt.show()

print 'W_1 = {}'.format(W_1.eval())
print 'b_1 = {}'.format(b_1.eval())

print 'W_2 = {}'.format(W_2.eval())
print 'b_2 = {}'.format(b_2.eval())

p = y.eval(feed_dict={x: np.linspace(0, 1, 1000).reshape((1000,1))})
plt.plot(p)
plt.show()

sess.close()







# sess = tf.Session()
# result = sess.run()
# print(result)